#!/bin/sh

IMAGE=$(docker build . | grep "Successfully built" | cut -d ' ' -f3) 
docker run $IMAGE
docker history $IMAGE >> history.txt
docker save $IMAGE >> layer.tar
