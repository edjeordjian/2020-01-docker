if [ $# -lt 1 ] ; then
	echo "Please specify a PORT as argument."
	exit 1
fi

echo 'LOCAL_PORT='$1 > .env
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml up
